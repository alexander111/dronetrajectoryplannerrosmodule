//////////////////////////////////////////////////////
//  DroneTrajectoryPlannerROSModuleNode.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 23, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>



// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"


//trajectory planner
#include "droneTrajectoryPlannerROSModule.h"


#include "nodes_definition.h"



#define _VERBOSE_DRONE_TRAJECTORY_PLANNER_ROSNODE


using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, MODULE_NAME_TRAJECTORY_PLANNER); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    cout<<"[ROSNODE] Starting Drone Trajectory Planner"<<endl;

    //Trajectory planner
    DroneTrajectoryPlanner2dROSModule MyDroneTrajectoryPlannerROSModule;
    MyDroneTrajectoryPlannerROSModule.open(n,MODULE_NAME_TRAJECTORY_PLANNER);

    // Async -> Spin
    ros::spin();


    return 0;
}

